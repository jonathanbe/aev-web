import donationRoutes from '../donation/routes';
import volunteerRoutes from '../volunteer/routes';

export default [
	{
		path: '/',
		component: require('./Home'),
		children: [
			...donationRoutes,
			...volunteerRoutes
		]
	}
]
