import Vue from 'vue';
import Directives from './directives';
import Components from './components/shared';
import NProgress from 'nprogress';
import VeeValidate from 'vee-validate';
import loadGoogleMapsApi from 'load-google-maps-api';

module.exports = (router, store, component) => {
	// Install shared plugins
	Vue.use(Directives);
	Vue.use(Components);

	// Execute router guards
	require('./config/guard')(router);

	// VueResource Request interceptors
	Vue.http.interceptors.push((request, next) => {
		// Setup the progress bar
		NProgress.start();
		next(response => {
			NProgress.done();
			return response;
		});
	});

	Vue.use(VeeValidate);

	// Init Google Maps
	loadGoogleMapsApi({
		key: process.env.GOOGLE_MAPS_KEY
	})
		// Init Vue entry component
		.then(googleMaps => new Vue({
			el: '#app',
			router,
			store,
			render: createElement => createElement(component)
		}));
}
