import gallery from '../modules/gallery';
import donation from '../modules/donation';
import volunteer from '../modules/volunteer';
import search from '../modules/search';
import logger from '../modules/logger';
import oauth from '../modules/oauth';
import blog from '../modules/blog';
import event from '../modules/event';
import location from '../modules/location';
import institution from '../modules/institution';
import page from '../modules/page';

export default {
	modules: {
		gallery,
		donation,
		volunteer,
		search,
		logger,
		oauth,
		blog,
		event,
		location,
		institution,
		page,
	}
}
