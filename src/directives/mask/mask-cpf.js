import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		if (!binding.value)
			return;

		VMasker(el).maskPattern('999.999.999-99');
	},

	unbind (el) {
		//
	}
}
