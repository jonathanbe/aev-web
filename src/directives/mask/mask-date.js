import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		if (!binding.value)
			return;

		VMasker(el).maskPattern('99/99/9999');
	},

	unbind (el) {
		//
	}
}
