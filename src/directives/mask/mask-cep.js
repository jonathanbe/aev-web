import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		if (!binding.value)
			return;

		VMasker(el).maskPattern('99999-999');
	},

	unbind (el) {
		//
	}
}
