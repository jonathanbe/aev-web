import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		if (!binding.value)
			return;

		var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
		VMasker(el).maskPattern(telMask[0]);
		el.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
	},

	unbind (el) {
		//
	}
}

function inputHandler(masks, max, event) {
	var c = event.target;
	var v = c.value.replace(/\D/g, '');
	var m = c.value.length > max ? 1 : 0;
	VMasker(c).unMask();
	VMasker(c).maskPattern(masks[m]);
	c.value = VMasker.toPattern(v, masks[m]);
}
