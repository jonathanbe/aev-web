export default {
	namespaced: true,

	state: {
		active: false,
		start: 1,
		images: []
	},

	getters: {
		isActive (state) {
			return state.active;
		},
		getImages (state) {
			return state.images;
		},
		getStart (state) {
			return state.start;
		}
	},

	mutations: {
		open (state, payload) {
			state.start = payload.index;
			state.images = payload.images;
			state.active = true;
		},

		close (state, payload) {
			state.active = false
			state.start = 1;
			state.images = [];
		}
	}
}
