import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,

	state: {
		institutions: [],
	},

	getters: {
		getInstitutions(state) {
			return state.institutions;
		}
	},

	actions: {
		fetchInstitutions({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				if (state.institutions.length > 0)
					return resolve(state.institutions)

				Vue.http.get(`${process.env.API_URL}/institutions`, {
					headers: getHeaders()
				})
				.then(response => {
					if (response.status == 200) {
						state.institutions = response.body.data;

						return resolve(state.institutions);
					}

					return reject(response);
				}, error => commit('logger/error', { message: 'Erro!' }));
			})
		},
	},

	mutations: {
		//
	}
}
