import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,
	state: {
		keyword: '',
		events: [],
		next_page_url: null,
		prev_page_url: null,
		current_page: 1
	},
	actions: {
		fetchEvents ({ state }, payload) {
			var uri = `${process.env.API_URL}/events`;

			if (state.keyword.length > 0)
				uri = `${process.env.API_URL}/events/search?q=${state.keyword}`

			return new Promise ((resolve, reject) => {
				Vue.http.get(uri, {
					headers: getHeaders()
				})
					.then(response => {
						console.log(response);
						if (response.status == 200) {
							state.events = response.body.data;
							state.prev_page_url = response.body.prev_page_url;
							state.next_page_url = response.body.next_page_url;

							return resolve(state.events);
						}

						return reject();
					})
			})
		},
		nextPage () {
		
		},
		getEvent (context, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/events/${payload.id}`,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200)
						return resolve(response.body)

					return reject();
				});
			})
		}
	},
	getters: {
		getEvents (state) {
			return state.events;
		}
	},
	mutations: {
		setKeyword (state, payload) {
			state.keyword = payload;
		}
	}
}
