import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,
	state: {
		keyword: '',
		posts: [],
		categories: [],
		selected: [],
		next_page_url: null,
		prev_page_url: null,
		current_page: 1
	},
	actions: {
		fetchCategories({state}, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(process.env.API_URL + '/categories', {
					headers: getHeaders()
				})
					.then(response => {
						if (response.status == 200) {
							state.categories = [];
							state.selected = [];
							state.categories.push(...response.body);
							state.selected.push(...response.body);

							return resolve();
						}

						return reject();
					}, error => console.error(error))
			})
		},

		fetchPosts ({ state }, payload) {
			var cats = state.selected.map(category => category.id);
			var uri = `${process.env.API_URL}/posts/search?q=${state.keyword}&cats=${cats}`;

			return new Promise((resolve, reject) => {
				Vue.http.get(uri, {
					headers: getHeaders()
				})
					.then(response => {
						if (response.status == 200) {
							state.posts = response.body.data;
							state.prev_page_url = response.body.prev_page_url;
							state.next_page_url = response.body.next_page_url;

							return resolve();
						}

						return reject();
					})
			})
		},
		nextPage () {
		
		},
		getPost (context, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(process.env.API_URL + '/posts/' + payload.id,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200)
						return resolve(response.body)

					return reject();
				});
			})
		}
	},
	getters: {
		getPosts (state) {
			return state.posts;
		},
		getCategories (state) {
			return state.categories;
		},
		getSelected (state) {
			return state.selected;
		}
	},
	mutations: {
		setKeyword (state, payload) {
			state.keyword = payload;
		},
		setSelected (state, payload) {
			state.selected = payload;
		}
	}
}
