var express = require('express'),
	request = require('request'),
	path = require('path');

let app = express(),
	port = process.env.PORT || 8080;

if (process.env.NODE_ENV == 'production') {
	// Apply gzip compression to js files
	app.get('*.js', function(req, res, next) {
		req.url = req.url + '.gz';
		res.setHeader('Content-Encoding', 'gzip');
		res.setHeader('Content-Type', 'text/javascript');
		next();
	});
}

// Routes
app.use('/', express.static(path.join(__dirname, 'public')));

// Request Access Token
app.post('/authorize', (req, res, next) => {
	var options = {
		method: 'POST',
		url: `${process.env.API_URL}/oauth2/token`,
		proxy: process.env.KONG_PROXY || 'https://kong:8443',
		tunnel: false,
		json: true,
		form: {
			grant_type: 'client_credentials',
			client_id: process.env.CLIENT_ID,
			client_secret: process.env.CLIENT_SECRET,
		}
	};

	return request(options, (err, _, body) => {
		if (err)
			next(err);

		return res.json(body);
	});
});

// Error middlware
app.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.send(err.message);

	console.log(err.stack);
});

// Listen to default port
app.listen(port, (err) => {
	if (err)
		console.error(`Error: ${err}`);

	console.log(`Server started ${port}`);
});

module.exports = app;
